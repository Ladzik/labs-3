﻿using System;
using Lab3;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(ICzas);
        public static Type I2 = typeof(IGrzanie);
        public static Type I3 = typeof(IProgram);

        public static Type Component = typeof(ModulGrzewczy);

        public static GetInstance GetInstanceOfI1 = (Component) => ModulGrzewczy.ZwrocCzas(new ModulGrzewczy());
        public static GetInstance GetInstanceOfI2 = (Component) => ModulGrzewczy.ZwrocGrzanie(new ModulGrzewczy());
        public static GetInstance GetInstanceOfI3 = (Component) => ModulGrzewczy.ZwrocProgram(new ModulGrzewczy());
        
        #endregion

        #region P2

        public static Type Mixin = typeof(CzasMixin);
        public static Type MixinFor = typeof(ModulGrzewczy);

        #endregion
    }
}
