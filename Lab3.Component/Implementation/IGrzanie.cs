﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public interface IGrzanie
    {
        void UstawTemperature(int temp);
        void Zmniejsz();
        void Zwieksz();
        void WlaczGrzanie();
        void WylaczGrzanie();
    }
}
