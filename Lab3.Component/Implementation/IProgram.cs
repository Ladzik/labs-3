﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public interface IProgram
    {
        void Uruchom(object Program);
        void ZmienProgram(object Program);
        void UstawProgram(object Program);
    }
}
