﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Implementation
{
    public interface ICzas
    {
        void Start();
        void Stop();
        void Zeruj();
    }
}
