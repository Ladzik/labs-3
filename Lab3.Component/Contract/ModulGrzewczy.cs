﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Implementation;

namespace Lab3.Contract
{
    public class ModulGrzewczy : ICzas, IProgram, IGrzanie
    {
        public static ICzas ZwrocCzas(ModulGrzewczy mg)
        {
            return (ICzas)mg;
        }

        public static IProgram ZwrocProgram(ModulGrzewczy mg)
        {
            return (IProgram)mg;
        }

        public static IGrzanie ZwrocGrzanie(ModulGrzewczy mg)
        {
            return (IGrzanie)mg;
        }

        void ICzas.Start()
        {
            throw new NotImplementedException();
        }

        void ICzas.Stop()
        {
            throw new NotImplementedException();
        }

        void ICzas.Zeruj()
        {
            throw new NotImplementedException();
        }

        void IProgram.Uruchom(object Program)
        {
            throw new NotImplementedException();
        }

        void IProgram.ZmienProgram(object Program)
        {
            throw new NotImplementedException();
        }

        void IProgram.UstawProgram(object Program)
        {
            throw new NotImplementedException();
        }

        void IGrzanie.UstawTemperature(int temp)
        {
            throw new NotImplementedException();
        }

        void IGrzanie.Zmniejsz()
        {
            throw new NotImplementedException();
        }

        void IGrzanie.Zwieksz()
        {
            throw new NotImplementedException();
        }

        void IGrzanie.WlaczGrzanie()
        {
            throw new NotImplementedException();
        }

        void IGrzanie.WylaczGrzanie()
        {
            throw new NotImplementedException();
        }
    }
}
